<?php

$params = parseParams(['coins', 'places', 'file'], $argv);

$errors = validateParams($params);
if (count($errors)) {
    die("Ошибка: " . join("\n", $errors));
}

generate($params);

function generate(array $params)
{
    $placesCount = $params['places'];
    $coinsCount = $params['coins'];
    $file = empty($params['file']) ? 'out.data' : $params['file'];

    /**
     * Используем тот факт, что есть такое размещение, допустим 3-x монет по 6 ячейкам,
     * в бинарном виде будет 111000, которое является таким числом,
     * что все остальные размещения в бинарном виде будут меньше этого числа.
     * Т.е. пробегаемся по всем числам начиная с 111000 до 1 и просто проверяем что,
     * если кол-во выставленных бит равно кол-ву монет - значит это число(размещение) нам подходит
     */
    $maxCode = str_pad('', $coinsCount, '1');
    $maxCode = str_pad($maxCode, $placesCount, '0');
    $maxCode = bindec($maxCode);

    $result = [];
    $variantsCount = 0;
    for ($i = $maxCode; $i > 0; $i--) {
        if (getBitCount($i) == $coinsCount) {
            $variantsCount++;
            $result[] = str_pad(decbin($i), $placesCount, '0', STR_PAD_LEFT);
        }
    }

    if ($variantsCount >= 10) {
        array_unshift($result, $variantsCount);
    } else {
        $result = ['менее 10 вариантов'];
    }

    if (file_put_contents($file, join("\n", $result))) {
        echo "В файл $file успешно записаны данные";
    }
}

function validateParams(array $params)
{

    if (empty($params)) {
        return ['Использование: php index.php --places=N --coins=M [--file=FILENAME]'];
    }

    $errors = [];

    $maxBits = strlen(decbin(PHP_INT_MAX));
    if (empty($params['places']) || $params['places'] <= 0) {
        $errors[] = 'Ячеек должно быть больше 0 (--places=N)';
    } elseif ($params['places'] > $maxBits) {
        $errors[] = "Ячеек должно быть не больше $maxBits (--places=N)";
    }

    if (empty($params['coins']) || $params['coins'] <= 0) {
        $errors[] = 'Монет должно быть больше 0 (--coins=N)';
    } elseif (!empty($params['places']) && $params['coins'] > $params['places']) {
        $errors[] = 'Монет должно быть не больше кол-ва ячеек';
    }

    return $errors;
}

function parseParams(array $allowed, array $params = [])
{
    $result = [];
    foreach ($params as $param) {
        $splited = explode('=', ltrim($param, '-'));
        if (count($splited) == 2 && in_array($splited[0], $allowed)) {
            $result[$splited[0]] = $splited[1];
        }
    }

    return $result;
}

function getBitCount($value)
{
    $count = 0;
    while ($value) {
        $count += $value & 1;
        $value = $value >> 1;
    }

    return $count;
}

